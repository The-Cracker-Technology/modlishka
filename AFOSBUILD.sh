rm -rf /opt/ANDRAX/modlishka

make

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make... PASS!"
else
  # houston we have a problem
  exit 1
fi

mkdir /opt/ANDRAX/modlishka

cp -Rf dist/proxy /opt/ANDRAX/modlishka/modlishka

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE 01... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf templates/ /opt/ANDRAX/modlishka/

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE 02... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
